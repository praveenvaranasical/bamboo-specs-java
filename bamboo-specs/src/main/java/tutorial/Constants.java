package tutorial;

public class Constants {

    // Linked Repository
    protected String repositoryName = "bamboospecsv1";
    protected String repositoryUrl = "https://bitbucket.org/praveenvaranasical/bamboo-specs-java.git";
    protected String repositoryBranch = "master";
    protected String gitUserName = "praveenvaranasical";
    protected String gitPassword = "5wE9Vn7yxP2AJFnrMnZV";

    protected String bambooServer = "http://localhost:8085";
    protected String repository = repositoryName;

    // Bamboo Project
    protected String bambooProjectName = "BambooSpecsProject";
    protected String bambooProjectKey = "BSJGP";
    protected String bambooPlanName = "javaWithGradle";
    protected String bambooPlanKey = "BSJGP";

    // Plan Permissions
    protected String username = "root";
    protected String groupname = "bamboo-admin";

    // Plan Variables
    protected String dockerRegistry = "docker.io";
    protected String dockerUsername = "praveenvp";
    protected String dockerPassword = "vvsdprasad";
    protected String sonarServer = "";
    protected String sonarUsername = "";
    protected String sonarPassword = "";
    protected String service = "plf-configuration-service";
    protected String serviceVersion = "v4";

    // tech Variables
    protected String techstack = "javaGradle"; // Acceptable values are: javaGradle, javaMaven
}
