package tutorial;

import com.atlassian.bamboo.specs.api.builders.applink.ApplicationLink;
// import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;

public class CreateLinkedRepository {

    public GitRepository gitRepository(String username, String password, String repoUrl, String repoName, String branch) {

        final UserPasswordAuthentication authentications = new UserPasswordAuthentication(username)
        .username(username)
        .password(password);

        final GitRepository gitRepository = new GitRepository()
        .name(repoName)
        .url(repoUrl)
        .branch(branch)
        .authentication(authentications);
        return gitRepository;
    }
}
