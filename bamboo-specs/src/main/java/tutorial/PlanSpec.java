package tutorial;

import tutorial.Constants;
import tutorial.CreateLinkedRepository;
import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.server.BitbucketServerRepository;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.builders.task.MavenTask;



@BambooSpec
public class PlanSpec {

    public static void main(final String[] args) throws Exception {

        Constants constantVariables = new Constants();
        
        // BambooServer bambooServer = new BambooServer("https://bamboo.ibdp.calibo.com");
        BambooServer bambooServer = new BambooServer(constantVariables.bambooServer);
        PlanSpec planspec = new PlanSpec();

        // Linked Repository
        final CreateLinkedRepository createLinkedRepository = new CreateLinkedRepository();
        final GitRepository gitRepository = createLinkedRepository.gitRepository(constantVariables.gitUserName, constantVariables.gitPassword, constantVariables.repositoryUrl, constantVariables.repositoryName, constantVariables.repositoryBranch);
        bambooServer.publish(gitRepository);

        if ( constantVariables.techstack.toLowerCase().equals("javagradle") ) {
            // Plan and Project Creation
            Plan plan = new PlanSpec().javaWithGradleStack();
            bambooServer.publish(plan);
        
            // Permissions 
            final PlanPermissions planPermission = planspec.planPermission(plan.getIdentifier());
            bambooServer.publish(planPermission);
        }
        else if ( constantVariables.techstack.toLowerCase().equals("javamaven") ) {
            System.out.println("I belong to java maven stack");
            // Plan and Project Creation
            Plan plan = new PlanSpec().javaWithMavenStack();
            bambooServer.publish(plan);
        
            // Permissions 
            final PlanPermissions planPermission = planspec.planPermission(plan.getIdentifier());
            bambooServer.publish(planPermission);
        }
        else {
            System.out.println("Please check the techstack accepted values and try with valid value again");
        }
    }

    public PlanPermissions planPermission(PlanIdentifier planIdentifier) {
        Constants constantPermissionVars = new Constants();
        final PlanPermissions planPermission = new PlanPermissions(new PlanIdentifier(planIdentifier.getProjectKey(), planIdentifier.getPlanKey()))
            .permissions(new Permissions()
                    .userPermissions(constantPermissionVars.username, PermissionType.EDIT, PermissionType.VIEW_CONFIGURATION, PermissionType.VIEW, PermissionType.ADMIN, PermissionType.CLONE, PermissionType.BUILD)
                    .loggedInUserPermissions(PermissionType.VIEW)
                    .groupPermissions(constantPermissionVars.groupname, PermissionType.ADMIN)
                    .anonymousUserPermissionView());
        return planPermission;
    }


    Project project() {
        Constants constantProjectVars = new Constants();
        return new Project()
                .name(constantProjectVars.bambooProjectName)
                .key(constantProjectVars.bambooProjectKey);
    }

    Plan javaWithGradleStack() {
        Constants constantPlanVars = new Constants();
        return new Plan(project(), constantPlanVars.bambooPlanName, constantPlanVars.bambooPlanKey)
                .description("Plan created using Bamboo Specs")
                .enabled(true)
                .stages(
                        new Stage("Checkout and Build")
                            .jobs(new Job("Checkout and Build", "RUN")
                                .tasks(
                                    new VcsCheckoutTask()
                                        .description("SourceCode Checkout")
                                        .checkoutItems(new CheckoutItem().defaultRepository()),
                                    new ScriptTask()
                                        .description("Build")
                                        .inlineBody("pwd\nls -la\necho 'Printing the Java Version'\njava -version\n./gradlew clean build -refresh-dependencies \n docker login -u ${bamboo.DOCKER_REGISTRY_USERNAME} -p ${bamboo.DOCKER_REGISTRY_PASSWORD} ${bamboo.DOCKER_REGISTRY}"),
                                    new DockerBuildImageTask()
                                        .description("Build Docker image")
                                        .imageName("${bamboo.DOCKER_REGISTRY}/${bamboo.DOCKER_REGISTRY_USERNAME}/${bamboo.SERVICE}:${bamboo.DOCKER_IMAGE_VERSION}")
                                        .useCache(true)
                                        .dockerfileInWorkingDir(),
                                    new ScriptTask()
                                        .description("Push Image to Repository")
                                        .inlineBody("echo \"Listing out the images\" \ndocker images \necho \"Pushing the images to the ${bamboo.DOCKER_REGISTRY}\" \ndocker push ${bamboo.DOCKER_REGISTRY}/${bamboo.DOCKER_REGISTRY_USERNAME}/${bamboo.SERVICE}:${bamboo.DOCKER_IMAGE_VERSION}")
                                        .enabled(true),
                                    // new DockerPushImageTask()
                                    //     .customRegistryImage("${bamboo.DOCKER_REGISTRY}/${bamboo.DOCKER_REGISTRY_USERNAME}/plf-configuration-service:v3")
                                    //     .authentication(constantPlanVars.dockerUsername, constantPlanVars.dockerPassword)
                                    //     .description("Push Docker image")
                                    //     .enabled(false),
                                    new ScriptTask()
                                        .description("Sonar scan")
                                        .inlineBody("echo \"Scanning the code using SonarQube\"\n ./gradlew sonarqube -Dsonar.verbose=true -Dsonar.projectKey=${bamboo.SERVICE} -Dsonar.projectName=${bamboo.SERVICE}  -Dsonar.java.binaries=build/classes  -Dsonar.host.url=\"${bamboo.SONAR_HOST}\" -Dsonar.login=${bamboo.SONAR_PASSWORD} --stacktrace")
                                        .enabled(false))
                                    // .workingSubdirectory("sourcecode"))))
                                .finalTasks(new ScriptTask()
                                    .description("Cleanup")
                                    .inlineBody("docker logout\n docker rmi $(docker images -aq) || true" ))))
                .linkedRepositories(constantPlanVars.repository)
                .variables(
                    new Variable("DOCKER_REGISTRY", constantPlanVars.dockerRegistry),
                    new Variable("DOCKER_REGISTRY_PASSWORD", constantPlanVars.dockerPassword),
                    new Variable("DOCKER_REGISTRY_USERNAME", constantPlanVars.dockerUsername),
                    new Variable("SERVICE", constantPlanVars.service),
                    new Variable("SONAR_HOST", constantPlanVars.sonarServer),
                    new Variable("SONAR_USERNAME", constantPlanVars.sonarUsername),
                    new Variable("SONAR_PASSWORD", constantPlanVars.sonarPassword),
                    new Variable("DOCKER_IMAGE_VERSION", constantPlanVars.serviceVersion))
                .forceStopHungBuilds();
    }

    Plan javaWithMavenStack() {
        Constants constantPlanVars = new Constants();
        return new Plan(project(), constantPlanVars.bambooPlanName, constantPlanVars.bambooPlanKey)
                .description("Plan created using Bamboo Specs")
                .enabled(true)
                .stages(
                        new Stage("Checkout and Build")
                            .jobs(new Job("Checkout and Build", "RUN")
                                .tasks(
                                    new VcsCheckoutTask()
                                        .description("SourceCode Checkout")
                                        .checkoutItems(new CheckoutItem().defaultRepository()),
                                    new ScriptTask()
                                        .description("Build")
                                        .inlineBody("pwd\nls -la\necho 'Printing the Java Version'\njava -version\nmvn clean install"),
                                    new MavenTask()
                                        .description("Build")
                                        .goal("clean install")
                                        .jdk("JDK 1.8.0_322 (JRE)")
                                        .executableLabel("maven3"),
                                    new DockerBuildImageTask()
                                        .description("Build Docker image")
                                        .imageName("${bamboo.DOCKER_REGISTRY}/${bamboo.DOCKER_REGISTRY_USERNAME}/${bamboo.SERVICE}:${bamboo.DOCKER_IMAGE_VERSION}")
                                        .useCache(true)
                                        .dockerfileInWorkingDir(),
                                    new ScriptTask()
                                        .description("Push Image to Repository")
                                        .inlineBody("echo \"Listing out the images\" \ndocker images \necho \"Pushing the images to the ${bamboo.DOCKER_REGISTRY}\" \ndocker push ${bamboo.DOCKER_REGISTRY}/${bamboo.DOCKER_REGISTRY_USERNAME}/${bamboo.SERVICE}:${bamboo.DOCKER_IMAGE_VERSION}")
                                        .enabled(true),
                                    new ScriptTask()
                                        .description("Sonar scan")
                                        .inlineBody("echo \"Scanning the code using SonarQube\"\n ./gradlew sonarqube -Dsonar.verbose=true -Dsonar.projectKey=${bamboo.SERVICE} -Dsonar.projectName=${bamboo.SERVICE}  -Dsonar.java.binaries=build/classes  -Dsonar.host.url=\"${bamboo.SONAR_HOST}\" -Dsonar.login=${bamboo.SONAR_PASSWORD} --stacktrace")
                                        .enabled(false))
                                    // .workingSubdirectory("sourcecode"))))
                                .finalTasks(new ScriptTask()
                                            .description("Cleanup")
                                            .inlineBody("docker logout\n docker rmi $(docker images -aq) || true" ))))
                .linkedRepositories(constantPlanVars.repository)
                .variables(
                    new Variable("DOCKER_REGISTRY", constantPlanVars.dockerRegistry),
                    new Variable("DOCKER_REGISTRY_PASSWORD", constantPlanVars.dockerPassword),
                    new Variable("DOCKER_REGISTRY_USERNAME", constantPlanVars.dockerUsername),
                    new Variable("DOCKER_IMAGE_VERSION", constantPlanVars.serviceVersion),
                    new Variable("SERVICE", constantPlanVars.service),
                    new Variable("SONAR_HOST", constantPlanVars.sonarServer),
                    new Variable("SONAR_USERNAME", constantPlanVars.sonarUsername),
                    new Variable("SONAR_PASSWORD", constantPlanVars.sonarPassword))   
                .forceStopHungBuilds();
    }
}
