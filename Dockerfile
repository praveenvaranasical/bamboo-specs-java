FROM openjdk:11-jre-slim
ENV context ""
ENV port 8095
ADD /src/main/resources/application.properties //

COPY /build/libs/*-1.0-SNAPSHOT.jar demo-1.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/demo-1.0-SNAPSHOT.jar","--server.port=${port}"]

